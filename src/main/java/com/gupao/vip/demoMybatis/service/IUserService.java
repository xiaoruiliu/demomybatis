package com.gupao.vip.demoMybatis.service;

import com.gupao.vip.demoMybatis.entity.User;

import java.util.List;

/**
 * Created by lenovo on 2019/6/1.
 */
public interface IUserService {
    List<User> list();
}
