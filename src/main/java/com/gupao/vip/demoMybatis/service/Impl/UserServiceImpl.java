package com.gupao.vip.demoMybatis.service.Impl;

import com.gupao.vip.demoMybatis.entity.User;
import com.gupao.vip.demoMybatis.mapper.UserMapper;
import com.gupao.vip.demoMybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lenovo on 2019/6/1.
 */
@Transactional
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> list() {
        System.out.println("进入service");
        List<User> list = userMapper.list();
        return list;
    }
}
