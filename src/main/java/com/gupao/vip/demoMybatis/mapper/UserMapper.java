package com.gupao.vip.demoMybatis.mapper;

import com.gupao.vip.demoMybatis.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lenovo on 2019/6/1.
 */
@Repository
public interface UserMapper {
    List<User> list();
}
