package com.gupao.vip.demoMybatis.annotation;

/**
 * Created by lenovo on 2019/6/2.
 */
/*描述一些信息，供程序使用*/
public interface MyAnnotation {
    //注解中的变量定义形式
    String name();
    int age();
}
