package com.gupao.vip.demoMybatis.controller;

import com.gupao.vip.demoMybatis.entity.User;
import com.gupao.vip.demoMybatis.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by lenovo on 2019/6/1.
 */
@RestController
@RequestMapping("test")
public class UserController {
    @Autowired
    private IUserService userService;

    @ResponseBody
    @RequestMapping("demoList")
    public List<User> userList(){
        List<User> list = userService.list();
        User user = new User();
        return list;
    }
}
