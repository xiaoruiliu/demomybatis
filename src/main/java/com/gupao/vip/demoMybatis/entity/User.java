package com.gupao.vip.demoMybatis.entity;


import lombok.Data;

/**
 * Created by lenovo on 2019/6/1.
 */
@Data
public class User {
    private int id;
    private String userName;
    private String passWord;
    private String realWord;
}
